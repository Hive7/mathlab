<?php

    // load essential build files

	require "modules/page-init.php";
	require "modules/navbar.php";

	$user = getSession("user");

	if (!$user) {
        header("Location: index.php");
		die();
	}
	
	// creates database connection

	require "modules/password.php";
	require "modules/database-commands.php";

	// collects necessary profile data to be displayed

	$query = "SELECT TEACHERID FROM TEACHERS WHERE USERID = ?";

	$statement = sqlstatement($database, $query, array(array("i", $user)));
	$statement->execute();
	$teacherId = $statement->get_result()->fetch_row()[0];

	$statement = sqlstatement($database, "SELECT CLASSCODE, CLASSNAME FROM CLASSMEMBERS WHERE USERID = ?", array(array("i", $user)));
	$statement->execute();
	$statement = $statement->get_result();

	$classes = array();
	
	while ($row = $statement->fetch_assoc()) {
		$classes[] = $row;
	}
	
	// generate basic page structure

	initHeader("Classes", $default . "<link rel=\"stylesheet\" href=\"css/class.css\" />\n\t<script type=\"text/javascript\" src=\"js/class.js\"></script>\n\t");
	makeNav();
	initContent();

?>
	<div class="table">
		<div class="table-header">
			<div class="left">
				<div class="title">Your Classes</div>
			</div>
			<div class="right">
				<a href="join-class.php">Join Class</a>
				<?php

					if ($teacherId !== NULL) {

				?>

				<a href="add-class.php">Add New</a>

				<?php

					}

				?>
				<select id="class" name="class">
					<option value="null">Please Select...</option>
					<?php
						
						for ($i = 0; $i < count($classes); $i++) {
							echo("<option value=\"" . $classes[$i]["CLASSCODE"] . "\">" . $classes[$i]["CLASSNAME"] . "</option>\n");
						}

					?>
				</select>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="table-body" id="table">
			<div class="individual row">Please select a class</div>
		</div>
	</div>
<?php

	// close off page

	initFooter();

?>