// Runs all commands stored when page loads to avoid overwritting onload function

window.onload = function () {
    for (var key in window.commands) {
        window.commands[key]();
    }
}