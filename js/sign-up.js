function triggerError(error) {

    // defines error codes sent into function

    var errorCodes = {
        "fr": "Full Name is required.\n",
        "fl": "Full Name has 60 character limit.\n",
        "fi": "Full Name can only contain letters.\n",
        "tr": "Target Grade is required.\n",
        "ur": "Username is required.\n",
        "ui": "Username must contain only letters, numbers and underscores.\n",
        "ul": "Username has 40 character limit.\n",
        "uu": "Username is already in use.\n",
        "er": "Email required.\n",
        "el": "Email has 40 character limit.\n",
        "ei": "Invalid email.\n",
        "eu": "Email is already in use",
        "pr": "Password is required.\n",
        "pl": "Password has 40 character limit.\n",
        "pi": "Passwords don't match.\n",
        "pm": "Password must be at least 6 characters.\n"
    },
        msg = "",
        codes = error.match(/.{1,2}/g);

    // converts codes to message

    for (var i = 0; i < codes.length; i++) {
        var code = codes[i];
        msg += errorCodes[code];
    }

    // shows modal containing error message

    presentModal("Error(s)", msg);
}

// command to verify inputs on sign up page to reduce moving between pages

window.commands.verify = function () {

    // attaches error checking to all inputs on change so live checking

    var inputs = document.forms.signup.getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++) {
        var element = inputs[i];

        // separate error checking for passwords

        if (element.name.indexOf("password") == -1) {
            element.addEventListener("input", function () {
                var fail = 0;

                // checks if HTML has regex tag to check value against

                if (this.getAttribute("regex") != null) {
                    if(!this.value.match(new RegExp(this.getAttribute("regex")))) {
                        fail = 1;
                    }
                }

                // length verification

                if (this.name != "fullname") {
                    if (this.value.length == 0 || this.value.length > 40) {
                        fail = 1;
                    }
                } else {
                    if (this.value.length == 0 || this.value.length > 60) {
                        fail = 1;
                    }
                }

                // changes border to tell user if edited fields are valid or not

                if (fail) {
                    this.style.borderColor = errorColor;
                } else {
                    this.style.removeProperty("border-color");
                }
            });
        } else {
            element.addEventListener("input", function () {
                var fail = 0;

                // checks if passwords are the same or different and affects both password box borders. Also compares length

                if (this.name == "c-password") {
                    if(this.value != this.form.password.value || this.value.length == 0) {
                        fail = 1;
                    }
                } else {
                    if(this.value != this.form["c-password"].value) {
                        fail = 2;
                    } else {
                        fail = 3;
                    }
                    if(this.value.length < 6 || this.value.length > 40) {
                        fail = 1;
                    }
                }
                if (fail == 3) {
                    this.form["c-password"].style.removeProperty("border-color");
                }
                if (fail == 1) {
                    this.style.borderColor = errorColor;
                } else {
                    if(fail == 2) {
                        this.form["c-password"].style.borderColor = errorColor;
                    }
                    this.style.removeProperty("border-color");
                }
            });
        }
    }

    // assigns error codes based on input values and checks for errors once form submitted
    // general length checks as well as insuring correct characters via regex

    document.forms.signup.onsubmit = function (e) {
        var error = "";
        if (this.fullname.value.length == 0) {
            error += "fr";
        } else if (this.fullname.value.length > 60) {
            error += "fl";
        } else if (!this.fullname.value.match(/^[a-zA-Z\s\-]*$/)) {
            error += "fi";
        }
        if (validGrades.indexOf(this.target.value) == -1) {
            error += "tr";
        }
        if (this.username.value.length == 0) {
            error += "ur";
        } else if (!this.username.value.match(/^[a-zA-Z0-9_]*$/)) {
            error += "ui";
        } else if (this.username.value.length > 40) {
            error += "ul";
        }
        if (this.email.value.length == 0) {
            error += "er";
        } else if (this.email.value.length > 40) {
            error += "el";
        } else if (!this.email.value.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
            error += "ei";
        }
        if (this.password.value.length == 0) {
            error += "pr";
        } else if (this.password.value.length < 6) {
            error += "pm"
        } else if (this.password.value.length > 40) {
            error += "pl";
        } else if (this.password.value != this["c-password"].value) {
            error += "pi";
        }
        if(error !== "") {
            triggerError(error);
            return false;
        }
    }
}