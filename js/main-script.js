window.commands.dropdown = function () {
	var dropdown = document.getElementById("dropdown"),
		selects = document.getElementsByTagName("select"),
		closes = document.getElementsByClassName("close");

	// Navbar dropdown clicker

	dropdown.onclick = function	() {
		if(this.className == "selected") {
			this.className = "";
		} else {
			this.className = "selected";
		}
	}

	// Selects change text color when value is given

	for (var i = 0; i < selects.length; i++) {
		var element = selects[i];
		element.onchange = function () {
			if(this.value !== "null") {
				this.style.color = "#000";
			} else {
				this.style.color = "#8E8E8E";
			}
		}
	}

	// Adds functionality to the close button

	for (var i = 0; i < closes.length; i++) {
		var element = closes[i];
		element.onclick = function () {
			this.parentNode.parentNode.removeChild(this.parentNode);
		}
	}
}