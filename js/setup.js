// Initialises global JavaScript variables

var modalShown = false,
    validGrades = ["Z", "A", "B", "C", "D", "E"],
    errorColor = "#D20000";

// Add modal as global function

function presentModal(titleText, content, callback) {
    if(!modalShown) {

        // ensures modal can only be in scope once at a time

        modalShown = true;

        // generates the elements that make up the modal

        var overlay = document.createElement("div"),
            modal = document.createElement("div"),
            inner = document.createElement("div"),
            title = document.createElement("div"),
            text = document.createElement("div"),
            endText = document.createElement("div"),
            bar = document.createElement("div");

        // adds definitions to elements so they can be styled in css
        
        overlay.className = "overlay";
        modal.className = "modal";
        title.className = "title";
        text.className = "text";
        endText.className = "end-text";
        bar.className = "break-line";
        inner.className = "inner";

        // gives content to elements

        title.innerText = titleText;
        text.innerText = content;
        endText.innerText = "Click anywhere to dismiss";

        // adds elements to scope

        inner.appendChild(title);
        inner.appendChild(bar);
        inner.appendChild(text);
        inner.appendChild(endText);

        modal.appendChild(inner);
        overlay.appendChild(modal);

        // adds removal of elements by clicking
        
        overlay.onclick = function () {
            modalShown = false;
            this.parentNode.removeChild(this);
            if (callback) {
                callback();
            }
        }
        document.body.appendChild(overlay);
    }
}

// initialises commands run onload

window.commands = {};