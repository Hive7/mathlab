function triggerError(error) {
    
    // defines error codes sent into function

    var errorCodes = {
        "uj": "User is already a member of this class.\n",
        "in": "Invalid class code password combination.\n",
    };

    // shows modal containing error message

    presentModal("Error", errorCodes[error]);
}