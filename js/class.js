var sortTeachers = function (dataset) {
    students = [];
    teachers = [];
    for(var i = 0; i < dataset.length; i++) {
        data = dataset[i];
        if(data.teacher) {
            teachers.push(data);
        } else {
            students.push(data);
        }
    }
    return [students, teachers];
}
var loadClass = function (data) {
    users = sortTeachers(JSON.parse(data));

    students = users[0];
    teachers = users[1];

    table = document.getElementById("table");
    table.innerHTML = "";

    table.innerHTML += "<div class=\"teachers table-header\"><div class=\"title left\">Teachers</div><div class=\"clearfix\"></div>";

    for(var i = 0; i < teachers.length; i++) {
        style = "";
        if (i % 2 == 0) {
            style = " style=\"background-color: #d4d4d4;\"";
        }
        table.innerHTML += "<div class=\"teacher row\"" + style + "><div class=\"fullname\">" + teachers[i].name + "</div><div class=\"username\">" + teachers[i].username + "</div></div>";
    }
    
    if (teachers.length == 0) {
        table.innerHTML += "<div class=\"individual row\">There are no teachers in this class</div>";
    }
    
    table.innerHTML += "</div><div class=\"students table-header\"><div class=\"title left\">Students</div><div class=\"clearfix\"></div>";

    for(var i = 0; i < students.length; i++) {
        style = "";
        if (i % 2 == 0) {
            style = " style=\"background-color: #d4d4d4;\"";
        }
        table.innerHTML += "<div class=\"student row\"" + style + "><a class=\"userid\" href=\"profile.php?user=" + students[i].id + "\">" + students[i].id + "</a><div class=\"fullname\">" + students[i].name + "</div><div class=\"username\">" + students[i].username + "</div><div class=\"target\">" + (students[i].target == "Z" ? "A*" : students[i].target) + "</div></div></div>";
    }
    
    if (students.length == 0) {
        table.innerHTML += "<div class=\"individual row\">There are no students in this class</div>";
    }

    table.innerHTML += "</div>";
}
req = null;
window.commands.class = function () {
    select = document.getElementById("class");
    prevFunc = select.onchange;
    select.onchange = function () {
        prevFunc.call(this);
        if(this.value !== "null") {
            if (req != null) {
                req = null;
            }

            req = new XMLHttpRequest();
            req.open("POST", "processes/load-class.php", true);
            req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");            

            req.onreadystatechange = function () {
                if(this.readyState == 4 && this.status == 200) {
                    loadClass(this.responseText);
                }
            }
            req.send("code=" + this.value);
        } else {
            table = document.getElementById("table");
            table.innerHTML = "<div class=\"individual row\">Please select a class</div>";
        }
    }
}