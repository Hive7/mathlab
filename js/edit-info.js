function triggerError(error) {

    // defines error codes sent into function

    var errorCodes = {
        "fr": "Full Name is required.\n",
        "fl": "Full Name has 60 character limit.\n",
        "fi": "Full Name can only contain letters.\n",
        "tr": "Target Grade is required.\n",
        "nc": "Content needs to be changed before you can save.\n"
    },
        msg = "",
        codes = error.match(/.{1,2}/g);

    // converts codes to message

    for (var i = 0; i < codes.length; i++) {
        var code = codes[i];
        msg += errorCodes[code];
    }

    // shows modal containing error message

    presentModal("Error(s)", msg);
}
window.commands.edit = function () {
    document.forms["user-form"]["name"].addEventListener("input", function () {
        if (this.value.length < 1 || this.value.length > 61 || !this.value.match(/^[a-zA-Z\s\-]*$/)) {
            this.style.borderColor = errorColor;
        } else {
            this.style.removeProperty("border-color");
        }
    });
    document.forms["user-form"].onsubmit = function (e) {
        if (target != this["target"].value || name != this["name"].value) {
            var error = "";
            if (!this["name"].value.match(/^[a-zA-Z\s\-]*$/) || !(this["name"].value.length > 0) || !(this["name"].value.length < 61)) {
                if (!(this["name"].value.length > 0)) {
                    error += "fr";
                } else if (!(this["name"].value.length < 61)) {
                    error += "fl";
                }
                if (!this["name"].value.match(/^[a-zA-Z\s\-]*$/)) {
                    error += "fi";
                }
            }
            if (validGrades.indexOf(this["target"].value) == -1) {
                error += "tr";
            }
            if (error == "") {
                return true;
            } else {
                triggerError(error);
            }
        } else {
            presentModal("Error(s)", "Content needs to be changed before you can save.", false);
        }
        return false;
    }
}

