var questionType = ["a", "b", "c"],

    // tracking of progress

    questionNumber = 0,
    correctAnswers = 0,

    // default defintions

    currentQuestion = "Question",

    // format [[answers 1-4], correct answer index]

    currentAnswers = [
        ["1.", "2.", "3.", "4."], 0
    ],
    answers = [],
    givenAnswers = [],
    symbols = {neg: "−", divide: "÷", mult: "×"},
    saved = false,
    redirect = false;

// Update values after event cycle

function updateValues() {
    document.getElementById("c-question").innerHTML = "Question: " + (questionNumber + 1) + "/" + questionType.length;
    document.getElementById("c-answer").innerHTML = correctAnswers;
    document.getElementById("question").innerHTML = currentQuestion;
    for (x = 0; x < 4; x++) {
        answers[x].children[0].innerHTML = currentAnswers[0][x];
    }
}

// Generate questions each event cycle

function generateQuestion() {
    answerVals = [];
    switch (questionType[questionNumber]) {
        case "a":

            // simple algebra

            q = "";

            // format: ax + b = c

            a = Math.floor(Math.random() * 31 - 15);
            b = Math.floor(Math.random() * 31 - 15);
            x = Math.floor(Math.random() * 31 - 15);
            
            // make sure value is non-zero

            a = a == 0 ? 1 : a;
            x = x == 0 ? 1 : x;

            // ensures the equation is not self solved

            while (b == 0 && a == 1) {
                a = Math.floor(Math.random() * 31 - 15);
                a = a == 0 ? 1 : a;
            }

            // calculate c to make sure answer is whole

            c = mathRep(a * x + b);
            a = mathRep(a);
            x = mathRep(x);
            q += a == "1" || a == symbols.neg + "1" ? (a == "1" ? "<i>x</i>" : symbols.neg + "<i>x</i>") : a + "<i>x</i>";
            q += b == 0 ? " = " + c : " " + posNegRep(b) + " = " + c;

            // add first answer

            answerVals.push("<i>x</i> = " + x);
            
            // fills in other answers

            for (var i = 0; i < 3; i++) {

                // makes sure no duplicates

                alt = x;
                while (answerVals.indexOf("<i>x</i> = " + alt) != -1 || alt == 0) {
                    alt = Math.floor(Math.random() * 31 - 15);
                }
                alt = mathRep(alt);
                answerVals.push("<i>x</i> = " + alt);
            }

            break;
        case "b":

            // factorisation

            q = "What is the fully factorised form of the equation: <span class=\"square\"><i>x</i><sup>2</sup></span> ";

            // format (x + a)(x + b) = x^2 + (a + b)x + (a * b)

            a = Math.floor(Math.random() * 21 - 10);
            b = Math.floor(Math.random() * 21 - 10);

            // non-zero values

            a = a == 0 ? 1 : a;
            b = b == 0 ? 1 : b;

            // finish question

            if (a + b != 0) {
                tmp = a + b;
                q += (posNegRep(tmp) == symbols.neg + " 1" || posNegRep(tmp) == "+ 1" ? posNegRep(tmp).substring(0, 2) : posNegRep(tmp)) + "<i>x</i> ";
            }

            tmp = a * b;

            q += posNegRep(tmp);

            // add correct answer

            answerVals.push("(<i>x</i> " + posNegRep(a) + ") (<i>x</i> " + posNegRep(b) + ")");

            // fill in other answers

            for (var i = 0; i < 3; i++) {
                alt = answerVals[0];

                // ensure no duplicates

                while(answerVals.indexOf(alt) != -1) {
                    c = Math.floor(Math.random() * 21 - 10);
                    d = Math.floor(Math.random() * 21 - 10);

                    // non-zero values

                    c = c == 0 ? 1 : c;
                    d = d == 0 ? 1 : d;

                    alt = "(<i>x</i> " + posNegRep(c) + ") (<i>x</i> " + posNegRep(d) + ")";
                }
                answerVals.push(alt);
            }
            
            break;
        case "c":

            // sequences

            q = "What is the <i>n</i><sup>th</sup> term of the sequence ";

            // format: an + b

            a = Math.floor(Math.random() * 11 - 5);
            b = Math.floor(Math.random() * 31 - 15);

            // non-zero a and b

            a = a == 0 ? 1 : a;
            b = b == 0 ? 1 : b;

            // generate 3-5 sequence values

            nums = [];
            vals = Math.floor(Math.random() * 3 + 3);

            for(var i = 0; i < vals; i++) {
                n = i + 1;
                nums[i] = mathRep(a * n + b);
            }

            a = oneNumRemoval(mathRep(a));

            // finish question

            nums = nums.join(", ");

            q += nums + "... ?";

            // add first answer

            answerVals.push(a + "<i>n</i> " + posNegRep(b));

            // fill in other answers

            for (var i = 0; i < 3; i++) {
                alt = answerVals[0];

                // prevent duplicates

                while (answerVals.indexOf(alt) != -1) {
                    c = Math.floor(Math.random() * 11 - 5);
                    d = Math.floor(Math.random() * 31 - 15);

                    c = c == 0 ? 1 : c;
                    d = d == 0 ? 1 : d;

                    c = oneNumRemoval(mathRep(c == 0 ? 1 : c));

                    alt = c + "<i>n</i> " + (d < 0 ? symbols.neg : "+") + " " + Math.abs(d);
                }

                answerVals.push(alt);
            }

            break;         
    }

    // setup initialise question for viewing

    currentQuestion = q;
    currentAnswers[1] = Math.floor(Math.random() * 4);
    currentAnswers[0][currentAnswers[1]] = answerVals[0];

    x = 1;

    for (var i = 0; i < 4; i++) {
        if (i != currentAnswers[1]) {
            currentAnswers[0][i] = answerVals[x];
            x++;
        } 
    }
}

// displays number in the format "+/- val" using correct symbols

function posNegRep (val) {
    return (val < 0 ? symbols.neg : "+") + " " + Math.abs(val);
}

// if value is 1 or -1 removes the 1 e.g. 1n = n

function oneNumRemoval(val) {
    if (val == symbols.neg + "1" || val == "1") {
        return val == "1" ? "" : symbols.neg;
    }
    return val;
}

// returns negative numbers with correct symbol

function mathRep (num) {
	return (num < 0 ? symbols.neg : "") + Math.abs(num);
}

// triggered when question answered

function answer(val) {

    // checks if last question just answered

    if (questionType[questionNumber + 1]) {

        // sets up value to be sent to be saved

        if (currentAnswers[0].indexOf(val) == currentAnswers[1]) {
            presentModal("Correct", (questionType.length - (questionNumber + 1)) + " question" + ((questionType.length - (questionNumber + 1)) == 1 ? "" : "s") + " left");
            correctAnswers++;
            givenAnswers.push("1");
            updateValues();
        } else {
            presentModal("Incorrect", (questionType.length - (questionNumber + 1)) + " question" + ((questionType.length - (questionNumber + 1)) == 1 ? "" : "s") + " remaining");
            givenAnswers.push("0");
        }

        // triggers next cycle as not last question

        questionNumber++;
        generateQuestion();
        updateValues();
    } else {

        // last question so doesn't generate new question but shows if the answer was correct or not

        cor = "Incorrect. ";
        givenAnswers.push("0");
        if (currentAnswers[0].indexOf(val) == currentAnswers[1]) {
            cor = "Correct. ";
            correctAnswers++;
            givenAnswers[givenAnswers.length - 1] = "1";
            updateValues();
        }

        // saves data if user is logged in

        if (userID) {
            var req = new XMLHttpRequest();
            req.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {

                    // makes sure to not redirect before data is saved

                    if (redirect) {
                        window.location.href = "index.php";
                    } else {
                        saved = true;
                    }
                }
            }
            req.open("POST", "processes/save-answers.php", true);
            req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            req.send("c=" + correctAnswers + "&q=" + questionType.join("") + "&a=" + givenAnswers.join(""));
        } else {
            saved = true;
        }
        presentModal("Test Complete", cor + "You have completed the quiz and your results saved if you're logged in. You will be redirected shortly after closing this dialogue.", function () {

            // makes sure to not redirect before data is saved

            if (saved) {
                window.location.href = "index.php";
            } else {
                redirect = true;
            }
        });
    }
}

// function to trigger the start of the quiz
// assigns trigger to answer question on answer click and starts first question

function startTest (questions) {

    // set questions to be included in the test

    tempArray = [];

    remaining = questions * 3 - (questions / 5 + 1) * 3;

    for (var i = 0; i < 3; i++) {
        for (var j = 0; j < questions / 5 + 1; j++) {
            tempArray.push(questionType[i]);
        }
        for (var j = 0; j < Math.floor(remaining * prevResults[i]); j++) {
            tempArray.push(questionType[i]);
        }
    }

    if (tempArray.length != questions * 3) {

        used = -1;

        iterations = (questions * 3 - tempArray.length);

        for(var i = 0; i < iterations; i++) {
            if (prevResults[0] >= prevResults[1] && used != 0 || used == 1) {
                if (prevResults[0] >= prevResults[2]) {
                    used = 0;
                } else {
                    used = 2;
                }
            } else if (prevResults[1] >= prevResults[2] && used != 1 || used == 2) {
                used = 1;
            } else {
                used = 2;
            }
            tempArray.push(questionType[used]);
        }     
    }

    questionType = tempArray;

    // randomize the ordering of the questions

    for (var i = 0; i < questionType.length; i++) {
        j = Math.floor(Math.random() * (questionType.length));
        while(j == i) {
            j = Math.floor(Math.random() * (questionType.length));
        }
        temp = questionType[i];
        questionType[i] = questionType[j];
        questionType[j] = temp;
    }

    // start the test

    answers = document.getElementsByClassName("answer");

    for (var i = 0; i < 4; i++) {
        answers[i].onclick = function() {
            updateValues();
            answer(this.children[0].innerHTML);
        }
    }
    generateQuestion();
    updateValues();

    // displays the test and hides the starting module after generation complete

    document.getElementById("quiz").style.display = "table";
    document.getElementById("quiz-options").style.display = "none";
}

// assigns events to question selection so test can be initiated

window.commands.test = function () {
    document.getElementById("q15").onclick = function () {
        startTest(5);
    }
    document.getElementById("q30").onclick = function () {
        startTest(10);
    }
};