window.commands.profile = function () {
    var items = document.getElementsByClassName("data-item"),
        tabs = document.getElementsByClassName("tab-title"),
        index = 0,
        newIndex = 0;

    // Adds functionality to tab system on profile page
    
    for (var i = 0; i < tabs.length; i++) {
        var tab = tabs[i];
        tab.onclick = function () {
            children = this.parentNode.children;
            for (var j = 0; j < children.length; j++) {
                var child = children[j];
                if(child == this) {
                    newIndex = j;
                    break;
                }               
            }
            if (newIndex != index) {
                tabs[index].className = "tab-title";
                items[index].className = "data-item";
                index = newIndex;
                item = items[index];
                this.className = "tab-title selected";
                item.className = "data-item selected";
            }
        }
    }
}