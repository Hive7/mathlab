function makeGraph (graph, content, column, title, targetGrade) {
	graph.getElementsByClassName("title")[0].innerHTML = title;
	points = graph.getElementsByClassName("points")[0];
	points.innerHTML = "";
	h = points.offsetHeight - 28;
	w = points.offsetWidth - 28;
	for(var i = 0; i < content.length; i++) {
		dateObject = new Date(content[i].datetime * 1000);
		height2 = (content[i][column] / 100) * h;
		if (i !== 0) {
			width = (w - 20 * content.length) / (content.length + 1);
			height1 = (content[i - 1][column] / 100) * h;
			height = height1 - height2;

			line = document.createElement("div");
			line.className = "line";
			line.style.height = height1 + "px";
			line.style.width = width + "px";

			bar = document.createElement("div");
			bar.className = "line-bar";
			bar.style.left = "-10px";
			bar.style.right = (Math.sqrt(((width + 20) * (width + 20)) + (height * height)) - 10 - width) * -1 + "px";
			bar.style.transform = "rotate(" + Math.atan(height / (width + 20)) / Math.PI * 180 + "deg)";

			line.appendChild(bar);
			points.appendChild(line);
		}

		point = document.createElement("div");
		point.className = "point";
		point.style.height = height2 + "px";

		dot = document.createElement("div");
		dot.className = "dot";
        background = "green"
        g = 80;
		switch (targetGrade) {
			case "Z":
				g = 80;
				break;
			case "A":
				g = 70;
				break;
			case "B":
				g = 60;
				break;
			case "C":
				g = 50;
				break;
			case "D":
				g = 40;
				break;
			case "E":
				g = 30;
				break;
		}
		o = g - 10;
		if (content[i][column] < o) {
			background = "red";
		} else if (content[i][column] < g) {
			background = "orange";
		}
		dot.style.backgroundColor = background;

		info = document.createElement("div");
		info.className = "info";
		info.innerHTML = "<div class=\"line\">Total: " + content[i]["score"] + "</div>" + "<div class=\"line\">Sequences: " + content[i]["seq"] + "</div>" + "<div class=\"line\">Factorisation: " + content[i]["fac"] + "</div>" + "<div class=\"line\">Algebra: " + content[i]["alg"] + "</div>";

		date = document.createElement("div");
		date.className = "date";
		date.innerHTML = standardDate(dateObject);
		
		dot.appendChild(info);
		point.appendChild(dot);
		point.appendChild(date);
		points.appendChild(point);
	}

	
}
function standardDate (dateObject) {
	var m = (dateObject.getMonth() + 1).toString(), d = dateObject.getDate().toString();
	return (d.length == 1 ? "0" + d : d) + "/" + (m.toString().length == 1 ? "0" + m.toString() : m) + "/" + dateObject.getFullYear().toString();
}

window.commands.graph = function () {
    graph = document.getElementsByClassName("graph")[0];
    makeGraph(graph, data, "score", "Total", targetGrade);
    opts = graph.getElementsByClassName("options")[0].getElementsByTagName("div");
    for (var i = 0; i < opts.length; i++) {
        opts[i].onclick = function () {
            makeGraph(graph, data, this.id, this.innerHTML, targetGrade);
            this.parentNode.getElementsByClassName("selected")[0].className = "";
            this.className = "selected";
        }
    }
}