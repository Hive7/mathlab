function triggerError(error) {
    
    // defines error codes sent into function

    var errorCodes = {
        "nr": "Class name is required.\n",
        "nl": "Class name has a 20 character limit.\n",

        "cr": "Class code is required.\n",
        "cl": "Class code has a 5 character limit.\n",
        "ct": "Class code is already in use.\n",

        "pr": "Password is required.\n",
        "pl": "Password has 40 character limit.\n",
        "pi": "Passwords don't match.\n",
        "pm": "Password must be at least 6 characters.\n"
    },
        msg = "",
        codes = error.match(/.{1,2}/g);

    // converts codes to message

    for (var i = 0; i < codes.length; i++) {
        var code = codes[i];
        msg += errorCodes[code];
    }

    // shows modal containing error message

    presentModal("Error(s)", msg);
}

window.commands.addclass = function () {
    inputs = document.forms["class"].getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++) {
        inputs[i].addEventListener("input", function () {
            var fail = 0;
            
            // checks if passwords are the same or different and affects both password box borders. Also compares length
            if (this.name.indexOf("password") != -1) {
                if (this.name == "c-password") {
                    if(this.value != this.form["password"].value || this.value.length == 0) {
                        fail = 1;
                    }
                } else if (this.name == "password") {
                    if(this.value != this.form["c-password"].value) {
                        fail = 2;
                    } else {
                        fail = 3;
                    }
                    if(this.value.length < 6 || this.value.length > 40) {
                        fail = 1;
                    }
                }
            } else {
                if (this.value.length > 20 || this.value.length == 0) {
                    fail = 1;
                } else if (this.name == "c-code" && this.value.length > 5) {
                    fail = 1;
                }
            }
            if (fail == 3) {
                this.form["c-password"].style.removeProperty("border-color");
            }
            if (fail == 1) {
                this.styl0e.borderColor = errorColor;
            } else {
                if(fail == 2) {
                    this.form["c-password"].style.borderColor = errorColor;
                }
                this.style.removeProperty("border-color");
            }
        });
    }
    document.forms["class"].onsubmit = function (e) {
        var error = "";
        
        if (this["c-name"].value.length == 0) {
            error += "nr";
        } else if (this["c-name"].value.length > 20) {
            error += "nl";
        }

        if (this["c-code"].value.length == 0) {
            error += "cr";
        } else if (this["c-code"].value.length > 5) {
            error += "cl";
        }

        if (this["password"].value.length == 0) {
            error += "pr";
        } else if (this["password"].value.length < 6) {
            error += "pm"
        } else if (this["password"].value.length > 40) {
            error += "pl";
        } else if (this["password"].value != this["c-password"].value) {
            error += "pi";
        }

        if(error !== "") {
            triggerError(error);
            return false;
        }
    }
}