<?php

    session_start();

    require "../modules/methods.php";

    $user = getSession("user");

	if (!$user) {
        header("Location: ../index.php");
		die();
	}
	
	// creates database connection

	require "../modules/password.php";
	require "../modules/database-commands.php";

	// collects necessary profile data to be displayed

	$query = "SELECT TEACHERID FROM TEACHERS WHERE USERID = ?";

	$statement = sqlstatement($database, $query, array(array("i", $user)));
	$statement->execute();
    $teacherId = $statement->get_result()->fetch_row()[0];
    
    if ($teacherId == NULL) {
        header("Location: ../index.php");
		die();
    }

    $name = postHeader("c-name");
    $code = postHeader("c-code");
    $pword = postHeader("password");
    $cpword = postHeader("c-password");

    if (!$code || !$name || !$pword || !$cpword) {
        header("Location: ../index.php");
        die();
    }

    $error = "";
    $exists = sqlstatement($database, "SELECT EXISTS(SELECT 1 FROM CLASSES WHERE CLASSCODE = ?)", array(array("s", $code)));
    $exists->execute();
    $exists = $exists->get_result()->fetch_row()[0];
    $exists = !!$exists;
    if (!$exists) {
        if (strlen($name) == 0) {
            $error .= "nr";
        } else if (strlen($name) > 20) {
            $error .= "nl";
        }

        if (strlen($code) == 0) {
            $error .= "cr";
        } else if (strlen($code) > 5) {
            $error .= "cl";
        }
        
        if (strlen($pword) == 0) {
            $error .= "pr";
        } else if (strlen($pword) < 6) {
            $error .= "pm";
        } else if (strlen($pword) > 40) {
            $error .= "pl";
        } else if ($pword != $cpword) {
            $error .= "pi";
        }

        if ($error == "") {
            error_reporting(0);
            $pword = crypt($pword);
            error_reporting(1);
            sqlstatement($database, "INSERT INTO CLASSES (TEACHERID, CLASSNAME, CLASSCODE, CLASSPASSWORD) VALUES (?, ?, ?, ?)", array(array("i", $teacherId), array("s", $name), array("s", $code), array("s", $pword)))->execute();
            sqlstatement($database, "INSERT INTO CLASSMEMBERS (USERID, CLASSCODE, CLASSNAME, TEACHER) VALUES (?, ?, ?, true)", array(array("i", $user), array("s", $code), array("s", $name)))->execute();
        } else {
            $_SESSION["error"] = $error;
            header("add-class.php");
            die();
        }
    }

    header("Location: ../index.php");
    die();

?>