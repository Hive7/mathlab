<?php

    session_start();

    // load essential build files

    require "../modules/methods.php";
    require "../modules/globals.php";

    // cancels if user not logged in

    if (!getSession("user")) {
        die();
    }

    // collects data sent into page from test

    $correct = postHeader("c");
    $questionTypes = postHeader("q");
    $givenAnswers = postHeader("a");

    // verifies data is all there

    if(!($correct && $questionTypes && $givenAnswers)) {
        die();
    }

    $alg = 0;
    $fac = 0;
    $seq = 0;

    // calculates users score for each topic

    for ($i = 0; $i < strlen($questionTypes); $i++) {
        if ($givenAnswers[$i] == "1") {
            if ($questionTypes[$i] == "a") {
                $alg++;
            } else if ($questionTypes[$i] == "b") {
                $fac++;
            } else {
                $seq++;
            }
        }
    }

    // converts score to percentage and sets score to -1 if doesn't exist

    if (substr_count($questionTypes, "a") > 0) {
        $alg = (int)($alg / substr_count($questionTypes, "a") * 100);
    } else {
        $alg = -1;
    }
    if (substr_count($questionTypes, "b") > 0) {
        $fac = (int)($fac / substr_count($questionTypes, "b") * 100);
    } else {
        $fac = -1;
    }
    if (substr_count($questionTypes, "c") > 0) {
        $seq = (int)($seq / substr_count($questionTypes, "c") * 100);
    } else {
        $seq = -1;
    }

    // calculates total percentage

    $tot = (int)($correct / strlen($questionTypes) * 100);

    // creates database connection

    require "../modules/password.php";
    require "../modules/database-commands.php";

    // inserts data into database

    $vars = array(array("i", getSession("user")), array("i", $tot), array("i", $alg), array("i", $fac), array("i", $seq), array("i", getDate()[0]));
    $insertScore = sqlstatement($database, "INSERT INTO SCORES (USERID, SCORE, ALG, FAC, SEQ, DATETIME) VALUES (?, ?, ?, ?, ?, ?)", $vars);
    $insertScore->execute();

?>