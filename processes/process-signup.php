<?php

    session_start();

    // load essential build files

    require "../modules/methods.php";
    require "../modules/globals.php";

    // loads different variables used in the signup process

    $full = postHeader("fullname"); 
    $username = postHeader("username");
    $target = postHeader("target");
    $email = postHeader("email");
    $password = postHeader("password");
    $cpassword = postHeader("c-password");

    $errormsg = "";

    // checks all the data being inputted is valid

    if ($full && $username && $target && $email && $password && $cpassword) {
        if(strlen($full) == 0) {
            $errormsg .= "fr";
        } else if (strlen($full) > 60) {
            $errormsg .= "fl";
        } else if (!preg_match('/^[a-zA-Z\s\-]*$/', $full)) {
            $errormsg .= "fi";
        }
        if(!in_array($target, $validGrades)) {
            $errormsg .= "tr";
        }
        if(strlen($username) == 0) {
            $errormsg .= "ur";
        } else if (!preg_match('/^[a-zA-Z0-9_]*$/', $username)) {
            $errormsg .= "ui";
        } else if (strlen($username) > 40) {
            $errormsg .= "ul";
        }
        if (strlen($email) == 0) {
            $errormsg .= "er";
        } else if (strlen($email) > 40) {
            $errormsg .= "el";
        } else if (!preg_match('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $email)) {
            $errormsg .= "ei";
        }
        if (strlen($password) == 0) {
            $errormsg .= "pr";
        } else if (strlen($password) < 6) {
            $errormsg .= "pm";
        } else if (strlen($password) > 40) {
            $errormsg .= "pl";
        } else if ($password != $cpassword) {
            $errormsg .= "pi";
        }
    } else {

        // if invalid data structure, sends back to signup page

        header("Location: ../sign-up.php");
        die();
    }

    if($errormsg != "") {

        // if error is found sends back to signup with the error attached

        $_SESSION["error"] = $errormsg;
        header("Location: ../sign-up.php");
        die();
    }

    // all data is valid so creates database connection

    require "../modules/password.php";
    require "../modules/database-commands.php";

    // checks if the user exists before trying to sign the user up

    $exists = sqlexists($database, "USERNAME = ? OR EMAIL = ?", "USERS", array(array("s", $username), array("s", $email)));

    if (!$exists) {

        // generates email confirmation code

        $confirmemail = generateConfirmation(30);

        // encrypts sensitive data for storage

        error_reporting(0);
        $password = crypt($password);
        $encryptConfirm = crypt($confirmemail);
        error_reporting(1);

        // inserts valid data into array

        $vars = array(array("s", $full), array("s", $target), array("s", $username), array("s", $email), array("s", $password), array("b", false), array("s", $encryptConfirm));
        $insertUser = sqlstatement($database, "INSERT INTO USERS (FULLNAME, TARGET, USERNAME, EMAIL, PASSWORD, CONFIRMED, CONFIRMSTRING) VALUES (?, ?, ?, ?, ?, ?, ?)", $vars);
        $insertUser->execute();

        // gets userid of entered value and assigns to user session

        $getId = sqlstatement($database, "SELECT USERID FROM USERS WHERE USERNAME = ?", array(array("s", $username)));
        $getId->execute();
        $_SESSION["user"] = $getId->get_result()->fetch_row()[0];

        // sends confirmation email to the user

        mailConfirm($email, $confirmemail, $username, $mysqlpass);
    } else {

        // if user exists check the error with the data before sending it back to signup with error attached

        $userExists = sqlexists($database, "USERNAME = ?", "USERS", array(array("s", $username)));
        if ($userExists) {
            $errormsg .= "uu";
            $mailExists = sqlexists($database, "EMAIL = ?", "USERS", array(array("s", $email)));
            if ($mailExists) {
                $errormsg .= "eu";
            }
        } else {
            $errormsg .= "eu";
        }
    }

    if($errormsg != "") {
        $_SESSION["error"] = $errormsg;
        header("Location: ../sign-up.php");
        die();
    }

    // sends logged in user back to home page

    header("Location: ../index.php");

?>