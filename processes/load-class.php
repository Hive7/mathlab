<?php

    session_start();

    require "../modules/methods.php";

    $code = postHeader("code");
    $user = getSession("user");

    if ($user == false || !$code) {
        die();
    }

	require "../modules/password.php";
    require "../modules/database-commands.php";
    
    $statement = sqlstatement($database, "SELECT CLASSCODE FROM CLASSMEMBERS WHERE USERID = ?", array(array("i", $user)));
    $statement->execute();
    $statement = $statement->get_result();

    $codes = array();

    while ($row = $statement->fetch_assoc()) {
        $codes[] = $row["CLASSCODE"];
    }

    if (!in_array($code, $codes)) {
        die();
    }

    $statement = sqlstatement($database, "SELECT USERID, TEACHER FROM CLASSMEMBERS WHERE CLASSCODE = ? ORDER BY USERID", array(array("s", $code)));
    $statement->execute();
    $statement = $statement->get_result();

    $users = array();
    
    while($row = $statement->fetch_assoc()) {
        $users[] = $row;
    }

    $classmembers = array();

    if (count($users) > 0) {
        
        $output = "[";

        for ($i = 0; $i < count($users); $i++) {
            $statement = sqlstatement($database, "SELECT USERNAME, TARGET, FULLNAME FROM USERS WHERE USERID = ?", array(array("i", $users[$i]["USERID"])));
            $statement->execute();
            $out = $statement->get_result()->fetch_row();

            $id = $users[$i]["USERID"];
            $teacher = $users[$i]["TEACHER"];
            $username = $out[0];
            $name = $out[2];
            $target = $out[1];

            $output .= "{\"id\": $id,\"teacher\": $teacher,\"username\": \"$username\",\"name\": \"$name\",\"target\": \"$target\"}";

            if ($i != count($users) - 1) {
                $output .= ","; 
            }
        }
        $output .= "]";
        echo $output;
    } else {
        echo "error";
    }

    

?>