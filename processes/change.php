<?php

    session_start();

    // load essential build files

    require "../modules/methods.php";
    require "../modules/globals.php";

    // redirects if user not logged in 

    if (!getSession("user")) {
        header("Location: ../index.php");
        die();
    }

    $prev = getSession("origin");

    if($prev) {
        unset($_SESSION["origin"]);
    }

    if($prev == "password") {
        $errormsg = "";

        $user = getSession("user");
        $opassword = postHeader("o-password");
        $npassword = postHeader("n-password");
        $cpassword = postHeader("c-password");

        if (strlen($npassword) == 0) {
            $errormsg .= "pr";
        } else if (strlen($npassword) < 6) {
            $errormsg .= "pm";
        } else if (strlen($npassword) > 40) {
            $errormsg .= "pl";
        } else if ($npassword != $cpassword) {
            $errormsg .= "pi";
        }

        if ($errormsg == "") {

            // creates connection with database
    
            require "../modules/password.php";
            require "../modules/database-commands.php";

            $query = "SELECT PASSWORD FROM USERS WHERE USERID = ?";
            $statement = sqlstatement($database, $query, array(array("i", $user)));
            $statement->execute();
            $password = $statement->get_result()->fetch_row()[0];

            error_reporting(0);
            if(crypt($opassword, $password) == $password) {

                $npassword = crypt($npassword);

                error_reporting(1);

                $update = sqlstatement($database, "UPDATE USERS SET PASSWORD = ? WHERE USERID = ?", array(array("s", $npassword), array("i", $user)));
                $update->execute();

                header("Location: ../profile.php");
                die();
            } else {
                $errormsg .= "pf";
                $_SESSION["error"] = $errormsg;
            }
        } else {
            $_SESSION["error"] = $errormsg;
        }
        header("Location: ../edit-password.php");
        die();
    } else if ($prev == "information") {
        $user = getSession("user");
        $name = postHeader("name");
        $target = postHeader("target");
        
        if ($name && $target) {
            $errormsg = "";
    
            // creates connection with database
    
            require "../modules/password.php";
            require "../modules/database-commands.php";
    
            $query = "SELECT TARGET, FULLNAME FROM USERS WHERE USERID = ?";
    
            $statement = sqlstatement($database, $query, array(array("i", $user)));
            $statement->execute();
            $userData = $statement->get_result()->fetch_row();
    
            if ($userData[0] != $target || $userData[1] != $name) {
                if(strlen($name) == 0) {
                    $errormsg .= "fr";
                } else if (strlen($name) > 60) {
                    $errormsg .= "fl";
                } else if (!preg_match('/^[a-zA-Z\s\-]*$/', $name)) {
                    $errormsg .= "fi";
                }
                if(!in_array($target, $validGrades)) {
                    $errormsg .= "tr";
                }
            } else {
                $errormsg .= "nc";
            }
    
            if ($errormsg == "") {
                $update = sqlstatement($database, "UPDATE USERS SET FULLNAME = ?, TARGET = ? WHERE USERID = ?", array(array("s", $name), array("s", $target), array("i", $user)));
                $update->execute();
                header("Location: ../profile.php");
                die();
            } else {
                $_SESSION["error"] = $errormsg;
            }
        }
        header("Location: ../edit-info.php");
        die();
    }
?>