<?php

    session_start();

    // load essential build files

    require "../modules/methods.php";
    require "../modules/globals.php";

    $user = getSession("user");

    if ($user) {
        header("Location: ../index.php");
        die();
    }

    $username = postHeader("user");
    $email = postHeader("email");

    require "../modules/password.php";
    require "../modules/database-commands.php";

    $exists = sqlexists($database, "USERNAME = ? AND EMAIL = ?", "USERS", array(array("s", $username), array("s", $email)));

    if ($exists) {
        // generates email confirmation code

        $resetpassword = generateConfirmation(30);

        // encrypts sensitive data for storage

        error_reporting(0);
        $encryptReset = crypt($resetpassword);
        error_reporting(1);

        // inserts valid data into array

        $query = sqlstatement($database, "UPDATE USERS SET RESET = ? WHERE EMAIL = ?", array(array("s", $encryptReset), array("s", $email)));
        $query->execute();

        // sends confirmation email to the user

        mailResetPassword($email, $resetpassword, $username, $mysqlpass);
    } else {
        header("Location: ../forgot-password.php");
        die();
    }

    header("Location: ../index.php");
    die();    

?>