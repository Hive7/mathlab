<?php

    session_start();

    require "../modules/methods.php";

    $username = getSession("username");
    $hashed = getSession("hashed");

    if (!($username && $hashed)) {
        header("Location: ../index.php");
        die();
    }

    require "../modules/password.php";
    require "../modules/database-commands.php";

    $statement = sqlstatement($database, "SELECT RESET, USERID FROM USERS WHERE USERNAME = ?", array(array("s", $username)));
    $statement->execute();
    $items = $statement->get_result()->fetch_row();
    $reset = $items[0];
    $userid = $items[1];

    if ($reset != $hashed) {
        header("Location: ../index.php");
        die();
    }
    $password = postHeader("n-password");
    $cpassword = postHeader("c-password");

    $errormsg = "";

    if (strlen($password) == 0) {
        $errormsg .= "pr";
    } else if (strlen($password) < 6) {
        $errormsg .= "pm";
    } else if (strlen($password) > 40) {
        $errormsg .= "pl";
    } else if ($password != $cpassword) {
        $errormsg .= "pi";
    }

    if ($errormsg != "") {
        $_SESSION["error"] = $errormsg;
        header("Location: ../new-password.php");
        die();
    } else {
        error_reporting(0);
        $encryptedPass = crypt($password);
        error_reporting(1);

        $statement = sqlstatement($database, "UPDATE USERS SET PASSWORD = ? WHERE USERNAME = ?", array(array("s", $encryptedPass), array("s", $username)));
        $statement->execute();
        $statement = sqlstatement($database, "UPDATE USERS SET RESET = NULL WHERE USERNAME = ?", array("s", $username));
        
        $_SESSION["user"] = $userid;
    }
    header("Location: ../index.php");
    unset($_SESSION["hashed"]);
    unset($_SESSION["username"]);

?>