<?php

    session_start();

    // load essential build files

    require "../modules/methods.php";
    require "../modules/globals.php";

    // redirects if user logged in already

    if (getSession("user")) {
        header("Location: ../index.php");
        die();
    }

    // loads username and password sent from login page

    $username = postHeader("username");
    $password = postHeader("password");

    // creates connection with database

    require "../modules/password.php";
    require "../modules/database-commands.php";

    // checks if the user  exists in the database

    $error = false;
    $exists = sqlexists($database, "USERNAME = ?", "USERS", array(array("s", $username)));

    if ($exists) {

        // if user exists collects password and compares encrypted values
        // if values are correct logs user in otherwise throws an error

        $statement = sqlstatement($database, "SELECT PASSWORD, USERID FROM USERS WHERE USERNAME = ?", array(array("s", $username)));
        $statement->execute();
        $res = $statement->get_result();
        $vals = $res->fetch_row();
        if (crypt($password, $vals[0]) == $vals[0]) {
            $_SESSION["user"] = $vals[1];
        } else {
            $error = true;
        }
    } else {
        $error = true;
    }

    // if there was an issue assigns an error message when sent back to login page

    if ($error) {
        $_SESSION["error"] = 1;
        header("Location: ../login.php");
        die();
    }
    if(getSession("origin") == "profile") {
        header("Location: ../profile.php?user" . $_SESSION["user"]);
        unset($_SESSION["origin"]);
        die();
    }
    header("Location: ../index.php");

?>