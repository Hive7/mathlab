<?php

    session_start();

    // load essential build files

    require "../modules/methods.php";
    require "../modules/globals.php";

    // redirects if user not logged in

    $user = getSession("user");

    if (!$user) {
        header("Location: ../index.php");
        die();
    }

    // loads username and password sent from login page

    $code = postHeader("c-code");
    $password = postHeader("password");

    // creates connection with database

    require "../modules/password.php";
    require "../modules/database-commands.php";

    // checks if the class exists in the database

    $error = "";
    $exists = sqlexists($database, "CLASSCODE = ? AND USERID = ?", "CLASSMEMBERS", array(array("s", $code), array("i", $user)));

    if (!$exists) {

        // if class exists collects password and compares encrypted values
        // if values are correct joins user to class

        $statement = sqlstatement($database, "SELECT CLASSPASSWORD, CLASSNAME FROM CLASSES WHERE CLASSCODE = ?", array(array("s", $code)));
        $statement->execute();
        $res = $statement->get_result();
        $vals = $res->fetch_row();
        error_reporting(0);
        if (crypt($password, $vals[0]) == $vals[0]) {
            $teacher = "false";
        
            if (sqlexists($database, "USERID = ?", "TEACHERS", array(array("i", $user)))) {
                $teacher = "true";
            }
            sqlstatement($database, "INSERT INTO CLASSMEMBERS (USERID, CLASSCODE, CLASSNAME, TEACHER) VALUES (?, ?, ?, " . $teacher . ")", array(array("i", $user), array("s", $code), array("s", $vals[1])))->execute();
        } else {
            $error = "in";
        }
        error_reporting(1);
    } else {
        $error = "uj";
    }

    // if there was an issue assigns an error message when sent back to login page

    if ($error != "") {
        $_SESSION["error"] = $error;
        header("Location: ../join-class.php");
        die();
    }
    header("Location: ../class.php");

?>