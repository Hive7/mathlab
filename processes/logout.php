<?php
	
	session_start();

	// if user logged in deletes their user session

	if (isset($_SESSION['user'])) {
		unset($_SESSION['user']);
	}
	header("Location: ../index.php");

	die();


?>