<?php

    session_start();

    // loads basic methods

    require "../modules/methods.php";

    // acquires confirmation code and userid

    $confirm = getHeader("q");
    $username = getHeader("u");

    // creates connection with database

    require "../modules/password.php";
    require "../modules/database-commands.php";

    // collects encrypted string from table

    $getId = sqlstatement($database, "SELECT USERID, CONFIRMSTRING FROM USERS WHERE USERNAME = ?", array(array("s", $username)));
    $getId->execute();
    $vars = $getId->get_result()->fetch_row();

    // compares confirm strings

    if (crypt($confirm, $vars[1]) != $vars[1]) {
        header("Location: ../index.php");
        die();
    }

    // if confirm strings match confirm the account

    $database->query("UPDATE USERS SET CONFIRMED = true, CONFIRMSTRING = '' WHERE USERID = " . $vars[0]);

    // logs in user and sets confirm bannner to be displayed

    $_SESSION["user"] = $vars[0];
    $_SESSION["confirmed"] = true;
    header("Location: ../index.php");
    die();

?>