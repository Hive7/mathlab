<?php

// load essential build files

require "modules/page-init.php";
require "modules/navbar.php";

// loads variables

$user = getSession("user");

// if no profile selected sends to login page or loggin in user's page

if(!$user) {
    header("Location: login.php");
    die();
}

$_SESSION["origin"] = "password";

// builds basic page structure

$errors = getSession("error");
if($errors) {
    unset($_SESSION["error"]);
    $errors = '<script type="text/javascript">
    window.commands.error = function () {
        triggerError("' . $errors . '");
    }
    </script>';
}

initHeader("Edit Your Information", $default . "\n\t<script type=\"text/javascript\" src=\"js/edit-password.js\"></script>\n\t<link rel=\"stylesheet\" href=\"css/edit.css\" />\n\t" . $errors);
makeNav();
initContent();

// adds prompt for user to take a test

?>

<div class="title">Edit Password</div>

<div class="card">
    <form class="user-information" id="user-form" action="processes/change.php" method="POST">
        <div class="form-block">
            <div class="left">
                <div class="label">Old Password:</div>
            </div>
            <div class="right">
                <input type="password" class="value" name="o-password" placeholder="Old Password..." />
            </div>
        </div>
        <div class="form-block">
            <div class="left">
                <div class="label">New Password:</div>
            </div>
            <div class="right">
                <input type="password" class="value" name="n-password" placeholder="New Password..." />
            </div>
        </div>
        <div class="form-block">
            <div class="left">
                <div class="label">Confirm Password:</div>
            </div>
            <div class="right">
                <input type="password" class="value" name="c-password" placeholder="Confirm Password..." />
            </div>
        </div>
        <div class="form-block">
            <div class="left"></div>
            <div class="right">
                <button type="submit">Save</button>
            </div>
        </div>
    </form>
</div>

<?php

initFooter();

?>