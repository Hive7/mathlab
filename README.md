# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

open source viewing of the MathLab project

### Who do I talk to? ###

oliver@cole2.com to contact me

### What does it do ###

The project is a website where you can take math tests to improve your maths skills. This is targeted very much at the GCSE year of maths but can be used by other year groups if they want to improve in some areas. The website allows you to create an account so that you can track your scores and also allows you to compare your scores with your friends with the profile viewer and friend storer. To view your progress you can look at your profile where there will be graphs showing the different tests that you've taken and your ability and overall improvement in different areas. It will use PHP, HTML, CSS and JavaScript. I haven't decided on any plugins that I'll use but I haven't had to use any so far (I remember marks are given for using them). Am considering using a JS graph drawer but could probably write one with a bit of effort. The website will be fully responsive so that it can be used on any device resolution as well.