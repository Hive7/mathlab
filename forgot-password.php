<?php

// load essential build files

require "modules/page-init.php";
require "modules/navbar.php";

// loads variables

$user = getSession("user");

// if no profile selected sends to login page or loggin in user's page

if($user) {
    header("Location: index.php");
    die();
}
// builds basic page structure

$errors = getSession("error");
if($errors) {
    unset($_SESSION["error"]);
    $errors = '<script type="text/javascript">
    window.commands.error = function () {
        triggerError("' . $errors . '");
    }
    </script>';
}

initHeader("Password Reset", $default . "\n\t<script type=\"text/javascript\" src=\"js/forgot-password.js\"></script>\n\t<link rel=\"stylesheet\" href=\"css/edit.css\" />\n\t" . $errors);
makeNav();
initContent();

// adds prompt for user to take a test

?>

<div class="title">Reset your password</div>

<p>You will be sent a password reset email in order to make sure that it's you.</p>

<div class="card">
    <form class="user-information" id="user-form" action="processes/forgot.php" method="POST">
        <div class="form-block">
            <div class="left">
                <div class="label">User:</div>
            </div>
            <div class="right">
                <input type="text" class="value" name="user" placeholder="Username..." />
            </div>
        </div>
        <div class="form-block">
            <div class="left">
                <div class="label">Email:</div>
            </div>
            <div class="right">
                <input type="text" class="value" name="email" placeholder="Email..." />
            </div>
        </div>
        <div class="form-block">
            <div class="left"></div>
            <div class="right">
                <button type="submit">Send</button>
            </div>
        </div>
    </form>
</div>

<?php

initFooter();

?>