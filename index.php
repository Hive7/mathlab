<?php

    // load essential build files

	require "modules/page-init.php";
	require "modules/navbar.php";

	// generate basic page structure

	initHeader("Home", $default);
	makeNav();
	initContent();

	/*$pos = strpos($_SERVER["REQUEST_URI"], "index");

	if ($pos !== false) {
		header("Location: $_SERVER[HTTP_HOST]/" . substr($_SERVER["REQUEST_URI"], 0, $pos));
		die();
	}*/
?>

<div class="title">Welcome</div>
<div class="text">Welcome to Mathlab, a GCSE maths testing site which allows students struggling with Maths for their GCSEs to brush up on some sections of their maths. Here at MathLab you can take tests, check progress and compete with your friends.</div>

<?php

	// adds test prompt for users to get access to the test feature

	initTestPrompt();

	// close off page

	initFooter();

?>