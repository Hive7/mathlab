<?php

// load essential build files

require "modules/page-init.php";
require "modules/navbar.php";

// loads variables

$user = getSession("user");

// if no profile selected sends to login page or loggin in user's page

if(!$user) {
    header("Location: login.php");
    die();
}

$_SESSION["origin"] = "information";

// creates database connection

require "modules/password.php";
require "modules/database-commands.php";

// collects necessary profile data to be displayed

$query = "SELECT TARGET, FULLNAME FROM USERS WHERE USERID = ?";

$statement = sqlstatement($database, $query, array(array("i", $user)));
$statement->execute();
$userData = $statement->get_result()->fetch_row();

// builds basic page structure

$errors = getSession("error");
if($errors) {
    unset($_SESSION["error"]);
    $errors = '<script type="text/javascript">
    window.commands.error = function () {
        triggerError("' . $errors . '");
    }
    </script>';
}

initHeader("Edit Your Information", $default . "\n\t<script type=\"text/javascript\">var target = \"" . $userData[0] . "\", name = \"" . $userData[1] . "\";</script>\n\t<script type=\"text/javascript\" src=\"js/edit-info.js\"></script>\n\t<link rel=\"stylesheet\" href=\"css/edit.css\" />\n\t" . $errors);
makeNav();
initContent();

// adds prompt for user to take a test

?>

<div class="title">Edit Information</div>

<div class="card">
    <form class="user-information" id="user-form" action="processes/change.php" method="POST">
        <div class="form-block">
            <div class="left">
                <div class="label">Fullname:</div>
            </div>
            <div class="right">
                <input type="text" class="value" name="name" value="<?php echo $userData[1]; ?>" />
            </div>
        </div>
        <div class="form-block">
            <div class="left">
                <div class="label">Target:</div>
            </div>
            <div class="right">
                <select id="target" name="target">
                    <option value="null">Please Select...</option>
                    <?php
                    if ($userData[0] == "Z") {
                    ?>
                    <option value="Z" selected="selected">A*</option>
                    <?php
                        } else {
                    ?>
                    <option value="Z">A*</option>
                    <?php
                        }
                        // cycle of all grades accepted
                        $selected = "";
                        for($i = 1; $i < count($validGrades); $i++) {
                            if ($userData[0] == $validGrades[$i]) {
                                $selected = "selected=\"selected\"";
                            }
                            echo '<option value="' . $validGrades[$i] . '" ' . $selected . '>' . $validGrades[$i] . '</option>
                            ';
                            $selected = "";
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-block">
            <div class="left"></div>
            <div class="right">
                <button type="submit">Save</button>
            </div>
        </div>
    </form>
</div>

<?php

initFooter();

?>