<?php

    // load essential build files

	require "modules/page-init.php";
	require "modules/navbar.php";

	$user = getSession("user");

	if (!$user) {
        header("Location: index.php");
		die();
	}
	
	// creates database connection

	require "modules/password.php";
	require "modules/database-commands.php";

	// collects necessary profile data to be displayed

	$query = "SELECT TEACHERID FROM TEACHERS WHERE USERID = ?";

	$statement = sqlstatement($database, $query, array(array("i", $user)));
	$statement->execute();
    $teacherId = $statement->get_result()->fetch_row()[0];
    
    if ($teacherId == NULL) {
        header("Location: index.php");
		die();
    }

    $error = getSession("error");
	if($error) {
		unset($_SESSION["error"]);
		$error = '<script type="text/javascript">
		window.commands.error = function () {
			triggerError("Error", "' . $error . '");
		}
		</script>';
	}
	
	// generate basic page structure

	initHeader("Classes", $default . "\n\t<script type=\"text/javascript\" src=\"js/add-class.js\"></script>\n\t<link rel=\"stylesheet\" href=\"css/edit.css\" />\n\t" . $error);
	makeNav();
	initContent();
	
?>

<div class="title">Add Class</div>

<div class="card">
    <form class="class" id="class" action="processes/process-class.php" method="POST">
        <div class="form-block">
            <div class="left">
                <div class="label">Class Name:</div>
            </div>
            <div class="right">
                <input type="text" class="value" name="c-name" placeholder="Class Name..." />
            </div>
        </div>
        <div class="form-block">
            <div class="left">
                <div class="label">Class Code:</div>
            </div>
            <div class="right">
                <input type="text" class="value" name="c-code" placeholder="Class Code..." />
            </div>
        </div>
        <div class="form-block">
            <div class="left">
                <div class="label">Class Password:</div>
            </div>
            <div class="right">
                <input type="password" class="value" name="password" placeholder="Class Password..." />
            </div>
        </div>
        <div class="form-block">
            <div class="left">
                <div class="label">Confirm Password:</div>
            </div>
            <div class="right">
                <input type="password" class="value" name="c-password" placeholder="Confirm Password..." />
            </div>
        </div>
        <div class="form-block">
            <div class="left"></div>
            <div class="right">
                <button type="submit">Create</button>
            </div>
        </div>
    </form>
</div>

<?php

    initFooter();   

?>