<?php

    // load essential build files

	require "modules/page-init.php";
	require "modules/navbar.php";

	// loads variables

	$user = getSession("user");
	$userProfile = getHeader("user");

	// if no profile selected sends to login page or loggin in user's page

	if(!$userProfile) {
		if ($user) {
			header("Location: profile.php?user=$user");
		} else {
			$_SESSION["origin"] = "profile";
			header("Location: login.php");
		}
	}

	// creates database connection

	require "modules/password.php";
	require "modules/database-commands.php";

	// collects necessary profile data to be displayed

	$query = "SELECT USERNAME, TARGET";

	if ($userProfile == $user) {
		$query .= ", FULLNAME, CONFIRMED, EMAIL";
	}

	$query .= " FROM USERS WHERE USERID = ?";

	$statement = sqlstatement($database, $query, array(array("i", $userProfile)));
	$statement->execute();
	$userData = $statement->get_result()->fetch_row();

	$statement = sqlstatement($database, "SELECT SCORE, ALG, FAC, SEQ, DATETIME FROM SCORES WHERE USERID = ? ORDER BY DATETIME LIMIT 25", array(array("i", $userProfile)));
	$statement->execute();
	$scoreDat = $statement->get_result();

	$scoreData = array();

	while ($row = $scoreDat->fetch_assoc()) {
		$scoreData[] = $row;
	}

	$extra = "";

	if (count($scoreData) > 0) {
		$extra .= "\n\t<script type=\"text/javascript\">var targetGrade = \"" . $userData[1] . "\",data = [";
		for ($i = 0; $i < count($scoreData); $i++) {
			$extra .= "{
					score: " . $scoreData[$i]["SCORE"] . ",
					alg: " . $scoreData[$i]["ALG"] . ",
					fac: " . $scoreData[$i]["FAC"] . ",
					seq: " . $scoreData[$i]["SEQ"] . ",
					datetime: " . $scoreData[$i]["DATETIME"] . "
				}";
			if ($i !== count($scoreData) - 1) {
				$extra .= ",";
			}
		}
		$extra .= "];</script>";
		$extra .= "\n\t<script type=\"text/javascript\" src=\"js/graph.js\"></script>\n\t<link rel=\"stylesheet\" href=\"css/graph.css\" />";
	} 

	// builds basic page structure

	initHeader("Your Profile", $default . "\n\t<script type=\"text/javascript\" src=\"js/profile.js\"></script>\n\t<link rel=\"stylesheet\" href=\"css/profile-style.css\" />" . $extra);
	makeNav();
	initContent();

	// adds prompt for user to take a test

	initTestPrompt();

?>

<div class="title">
	<?php 
		
		// if user logged in welcomes them to profile with firstname otherwise greets with username also gets data presentable

		if ($userData) {
			if (count($userData) == 5) {

				// translate data for presentation

				if($userData[3]) {
					$userData[3] = "true";
				} else {
					$userData[3] = "false";
				}
				if($userData[1] == "Z") {
					$userData[1] = "A*";
				}

				// hide full email
				
				$userData[4] = maskEmail($userData[4]);

				echo "Welcome " . explode(" ", $userData[2])[0];
			} else {
				echo $userData[0] . "'s Profile";
			}
		} else {
			echo "Invalid User</div>";
			initFooter();
			die();
		}

	?>
</div>
<?php

	if (count($scoreData)) {

?>
<div class="graph">
	<div class="graph-container">
		<div class="options">
			<div id="score" class="selected">Total</div>
			<div id="seq">Sequences</div>
			<div id="alg">Algebra</div>
			<div id="fac">Factorisation</div>
		</div>
		<div class="title"></div>
		<div class="inner">
			<div class="y-axis">
				<div class="y-labels">
					<div class="y-label">100%</div>
					<div class="y-label">90%</div>
					<div class="y-label">80%</div>
					<div class="y-label">70%</div>
					<div class="y-label">60%</div>
					<div class="y-label">50%</div>
					<div class="y-label">40%</div>
					<div class="y-label">30%</div>
					<div class="y-label">20%</div>
					<div class="y-label">10%</div>
					<div class="y-label">0%</div>
				</div>
			</div>
			<div class="x-axis"></div>
			<div class="points"></div>
		</div>
	</div>
</div>

<?php

	} else {

?>
<div class="score-error">no scores</div>
<?php

	}	

	if (count($userData) == 5) {

?>

<div class="title">Personal Information</div>
<div class="text">This is the information that MathLab has regarding you. If any of it is inaccurate you can change it with the edit button below if you have confirmed your email. You can also change your password here with the link below. Due to security reasons we cannot show your whole email but we should have left enough for you to identify it as your own. If you have any issues don't hesitate to contact us.</div>
<a href="edit-info.php" class="edit-info">Edit information</a>
<a href="edit-password.php" class="edit-password">Change password</a>
<div class="user-information">
	<div class="labels">
		<?php

			for ($i = 0; $i < count($userInformationLabels); $i++) { 
				echo("<div class=\"label\">" . $userInformationLabels[$i] . ":</div>\n");
			}

		?>
	</div>
	<div class="values">

		<?php

			for ($i = 0; $i < count($userInformationOrder); $i++) { 
				echo("<div class=\"value\">" . $userData[$userInformationOrder[$i]] . "</div>\n");
			}

		?>

	</div>
</div>

<?php

	}

	// closes out page

	initFooter();

?>