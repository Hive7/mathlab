<?php

    require "modules/page-init.php";
    require "modules/navbar.php";

    // load essential build files

    $user = getSession("user");

    if ($user) {
        header("Location: index.php");
        die();
    }

    $username = getHeader("u");
    $reset = getHeader("q");
    $hasher = $reset;

    if (!$username || !$reset) {
        $username = getSession("username");
        $hasher = getSession("hashed");
    }

    require "modules/password.php";
    require "modules/database-commands.php";

    $statement = sqlstatement($database, "SELECT RESET FROM USERS WHERE USERNAME = ?", array(array("s", $username)));
    $statement->execute();
    $resetHash = $statement->get_result()->fetch_row()[0];

    if (!$resetHash) {
        header("Location: index.php");
        die();
    }

    error_reporting(0);
    $hashed = crypt($reset, $resetHash);
    error_reporting(1);

    if ($hashed != $resetHash && $hasher != $resetHash) {
        header("Location: index.php");
        die();
    }

    $_SESSION["hashed"] = $hashed;
    $_SESSION["username"] = $username;

    if(!$reset) {
        $_SESSION["hashed"] = $hasher;
    }

    $errors = getSession("error");
    if($errors) {
        unset($_SESSION["error"]);
        $errors = '<script type="text/javascript">
        window.commands.error = function () {
            triggerError("' . $errors . '");
        }
        </script>';
    }

    initHeader("Password Reset", $default . "\n\t<script type=\"text/javascript\" src=\"js/edit-password.js\"></script>\n\t<link rel=\"stylesheet\" href=\"css/edit.css\" />\n\t" . $errors);
    makeNav();
    initContent();

// adds prompt for user to take a test

?>

<div class="title">Reset your password</div>

<div class="card">
    <form class="user-information" id="user-form" action="processes/reset.php" method="POST">
        <div class="form-block">
            <div class="left">
                <div class="label">Password:</div>
            </div>
            <div class="right">
                <input type="password" class="value" name="n-password" placeholder="New Password..." />
            </div>
        </div>
        <div class="form-block">
            <div class="left">
                <div class="label">Confirm Password:</div>
            </div>
            <div class="right">
                <input type="password" class="value" name="c-password" placeholder="Confirm Password..." />
            </div>
        </div>
        <div class="form-block">
            <div class="left"></div>
            <div class="right">
                <button type="submit">Reset</button>
            </div>
        </div>
    </form>
</div>

<?php

initFooter();

?>


?>