<?php

    // load essential build files

    require "modules/page-init.php";
    require "modules/navbar.php";

    // generate basic page structure

    initHeader("About", $default);
    makeNav();
    initContent();

?>
    <div class="title">About Me</div>
    <div class="text">My name is Oliver Cole, and this project was made for my Computer Science coursework for my A Level when I was 17 years old. It is a website that allows you to take generated Maths tests which are completely random, stores your stats and allows you to view your progress.</div>
    <div class="title">What languages did I use?</div>
    <div class="text">For this project I used a mix of HTML, CSS, JavaScript, PHP and MYSQL. I used HTML, CSS and PHP for the front end generation and styling. Using PHP with this purpose allowed me to greatly reduce the amount of code that I had to write due to repeated elements being generated onto the page. For the runtime and error checking, I used PHP and JavaScript for the fastest performance with the JavaScript and additional checks through the PHP to ensure data wasn't duplicated. Finally, for the backend access and data storage, I used PHP and MYSQL. This allowed me to password lock most of the data and never have any sensitive information loaded into the page as it is compiled in the server before outputted.</div>
    <div class="text">When developing this project I used a program called XAMPP in order to run a server on my local machine and run the MYSQL. This allowed me to develop my application on the go.</div>
    <div class="title">What plugins did I use?</div>
    <div class="text">In the making of this project I chose to not use any extra PHP libraries down to personal preference and support over different devices.</div>
    <div class="title">How did I plan the project?</div>
    <div class="text">First of all, I designed the data structure of the project and wrote out the SQL commands that I would be using to gather certain data. I also drew a generalised version of what I wanted the site to look like overall. This was very useful for me as it gave me a direction for what I want to do in terms of making the front end of the project while still thinking about structuring the backend. I then quickly wrote a basic front end model for the website to be used through and later on added small intricacies to make the design more user friendly. After this it was a case of taking the data that the user had inputted and putting it into my data structure so they could use the application properly.</div>
    <div class="text"> I designed the profile system of the website such that anyone could look at another person's profile, even if they weren't logged in. So that people using the website can share their ID's with their friends and check each other's progress. The data displayed was loaded in such a way that sensitive data to the user is never displayed unless the user is logged in and on their own profile page.</div>
<?php

    // close off page

	initFooter();

?>