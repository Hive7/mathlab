<?php

    // load essential build files

	require "modules/page-init.php";
	require "modules/navbar.php";

	$user = getSession("user");

	if (!$user) {
        header("Location: index.php");
		die();
	}
	
	// creates database connection

	require "modules/password.php";
    require "modules/database-commands.php";
    
    $error = getSession("error");
	if($error) {
		unset($_SESSION["error"]);
		$error = '<script type="text/javascript">
		window.commands.error = function () {
			triggerError("' . $error . '");
		}
		</script>';
	}
	
	// generate basic page structure

	initHeader("Join Class", $default . "\n\t<script type=\"text/javascript\" src=\"js/join.js\"></script>\n\t<link rel=\"stylesheet\" href=\"css/edit.css\" />\n\t" . $error);
	makeNav();
	initContent();
	
?>

<div class="title">Join Class</div>

<div class="card">
    <form class="class" id="class" action="processes/process-join.php" method="POST">
        <div class="form-block">
            <div class="left">
                <div class="label">Class Code:</div>
            </div>
            <div class="right">
                <input autocomplete="false" type="text" class="value" name="c-code" placeholder="Class Code..." />
            </div>
        </div>
        <div class="form-block">
            <div class="left">
                <div class="label">Class Password:</div>
            </div>
            <div class="right">
                <input type="password" class="value" name="password" placeholder="Class Password..." />
            </div>
        </div>
        <div class="form-block">
            <div class="left"></div>
            <div class="right">
                <button type="submit">Join</button>
            </div>
        </div>
    </form>
</div>

<?php

    initFooter();   

?>