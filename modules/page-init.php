<?php

	session_start();

	require "modules/globals.php";

	// removes redirect route
	
	if(isset($_SESSION["origin"])) {
		if(!strpos($_SERVER["REQUEST_URI"], "login.php") && $_SESSION["origin"] == "profile") {
			unset($_SESSION["origin"]);
		} else if ($_SESSION["origin"] == "password" || $_SESSION["origin"] == "information") {
			unset($_SESSION["origin"]);
		}
	}

	// generates the start of each page to remove having to do for each page

	function initHeader ($title, $resources) {
		if (!isset($resources)) {
			$resources = "";
		}
		echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
	<meta charset=\"UTF-8\">
	<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
	<title>$title</title>
	<script type=\"text/javascript\" src=\"js/setup.js\"></script>
	$resources
	<script type=\"text/javascript\" src=\"js/run.js\"></script>
</head>
<body>
		";
	}

	// generates starting section of each page

	function initContent () {
		echo '	
	<div class="wrapper wrapper-main">
		<div class="background">
			<div class="content">
		';
	}

	// makes footer for each page

	function initFooter () {
		$copyright = "2017";
		if (date("Y") != "2017") {
			$copyright .= " - " . date("Y");
		} 
		echo '			</div>
		</div>
	</div>

	<div class="footer">
		<div class="footer-container">
			<div class="footer-content">Copyright &copy; Oliver Cole ' . $copyright . '</div>
		</div>
	</div>

</body>
</html>';
	}

	// ends page if no footer required on page

	function noFooter () {
		echo '			</div>
		</div>
	</div>

</body>
</html>';
	}

	// adds prompt for user to take a test

	function initTestPrompt () {
		echo '<div class="card card-1 prompt">
    <div class="title">Take a test</div>
    <div class="text">';
            if(getSession("user")) {
                echo "You're all set up with an account! To start taking tests click on the button below.";
            } else {
                echo "You haven't made an account yet, if you want to take a test still click the button below. If you want to make a free account to unlock full features click <a href='login.php'>here</a>";
            }
echo "    </div>
	<a href='test.php' class='test-button'>
		Test Me
	</a>
</div>";
	}

?>