<?php

    // defines global variables for PHP usage

    $default = '<link rel="stylesheet" href="css/main-style.css" />' . "\n\t" . '<link rel="stylesheet" href="css/modal-style.css" />' . "\n\t" . '<script type="text/javascript" src="js/main-script.js"></script>';
	$validGrades = array("Z", "A", "B", "C", "D", "E");
    $userInformationLabels = array("Fullname", "Username", "Email", "Target", "Email Confirmed");
    $userInformationOrder = array(2, 0, 4, 1, 3);

?>