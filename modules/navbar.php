<?php

	// make navbar

	require "modules/methods.php";

	function makeNav () {
		$user = getSession('user');
		echo '
	<div class="navbar">
		<div class="nav-inner clearfix">
			<div class="nav-container">
				<div class="title">
					<a href="index.php">MathLab</a>
				</div>
				<div id="dropdown">
					<div class="bar"></div>
					<div class="bar"></div>
					<div class="bar"></div>
				</div>
				<ul class="nav-items">
					<li class="nav-item">
						<a href="index.php">Home</a>
					</li>
					<li class="nav-item">
						<a href="profile.php?user=' . $user . '">Profile</a>
					</li>
					<li class="nav-item">';
				// changes button depending on logged in or not

	if ($user != false) {
		echo '
						<a href="class.php">Class</a>
					</li>	
					<li class="nav-item">
						<a href="processes/logout.php">Logout</a>';
	} else {
		echo '
						<a href="about.php">About</a>
					</li>	
					<li class="nav-item">
						<a href="login.php">Login</a>';
	}
	echo '			
				</ul>
			</div>
		</div>';
	
	// adds confirmation banner if user just confirmed their email
	
	if (getSession("confirmed")) {
		unset($_SESSION["confirmed"]);
		echo '
		<div class="banner">Thank you for confirming your email.<div class="close">×</div></div>
	';
	}
	echo '
	</div>
		';
	}

?>