<?php

    require_once "Mail.php";

    /*

    Database table structure

    CREATE TABLE IF NOT EXISTS USERS (USERID INTEGER PRIMARY KEY AUTO_INCREMENT, USERNAME VARCHAR(40), EMAIL VARCHAR(40), FULLNAME VARCHAR(60), PASSWORD VARCHAR(40), TARGET CHAR(1), CONFIRMED BOOLEAN, RESET VARCHAR(40), CONFIRMSTRING VARCHAR(40));
    CREATE TABLE IF NOT EXISTS SCORES (SCOREID INTEGER PRIMARY KEY AUTO_INCREMENT, USERID INTEGER, SCORE INTEGER, ALG INTEGER, FAC INTEGER, SEQ INTEGER, DATETIME INTEGER);
    CREATE TABLE IF NOT EXISTS TEACHERS (TEACHERID INTEGER PRIMARY KEY AUTO_INCREMENT, USERID INTEGER);
    CREATE TABLE IF NOT EXISTS CLASSES (CLASSID INTEGER PRIMARY KEY AUTO_INCREMENT, TEACHERID INTEGER, CLASSNAME VARCHAR(20), CLASSCODE VARCHAR(5), CLASSPASSWORD VARCHAR(40));
    CREATE TABLE IF NOT EXISTS CLASSMEMBERS (MEMBERID INTEGER PRIMARY KEY AUTO_INCREMENT, USERID INTEGER, CLASSCODE VARCHAR(5), CLASSNAME VARCHAR(20), TEACHER BOOL);

    */

    // create database connection

    $database = mysqli_connect('localhost', 'root', $mysqlpass, 'mathlab') or die('Error connecting to MySQL server.');

    // split inputted variables in values taken by database commands

    function splitVariables($vars) {
        $type = "";
        $vals = array();
        for ($i = 0; $i < count($vars); $i++) { 
            $type .= $vars[$i][0];
            array_push($vals, $vars[$i][1]);
        }
        return array($type, $vals);
    }

    // generates statement object

    function sqlstatement ($db, $query, $variables) {
        $statement = $db->prepare($query);
        $vars = splitVariables($variables);
        $statement->bind_param($vars[0], ...$vars[1]);
        return $statement;
    }

    // check whether user exists

    function sqlexists ($db, $query, $table, $variables) {
        $exists = sqlstatement($db, "SELECT EXISTS(SELECT 1 FROM " . $table . " WHERE " . $query . ")", $variables);
        $exists->execute();
        $res = $exists->get_result();
        $num = $res->fetch_row()[0];
        return !!$num;
    }

    // function to generate email confirmation code

    function generateConfirmation($length = 12) {
        $letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $symbols = "_.$/\*()!@'^";
        $output = "";
        $letlen = strlen($letters);
        $symlen = strlen($symbols);
        for ($i = 0; $i < $length; $i++) { 
            if(!!rand(0, 1)) {
                $output .= mb_substr($letters, rand(0, $letlen - 1), 1);
            } else {
                $output .= mb_substr($symbols, rand(0, $symlen - 1), 1);
            }
        }
        return $output;
    }

    // sends email to user that just signed up so they can confirm their email

    function sendMail($to, $subject, $message, $password) {

        $from = "mathlab.pass@gmail.com";
        $host = "ssl://smtp.gmail.com";
        $port = "465";
        $username = 'mathlab.pass@gmail.com';

        $headers = array('From' => $from, 'To' => $to, 'Subject' => $subject, 'Content-Type' => "text/html; charset=ISO-8859-1");

        $smtp = Mail::factory('smtp', array(
            'host' => $host,
            'port' => $port,
            'auth' => true,
            'username' => $username,
            'password' => $password)
        );
        $mail = $smtp->send($to, $headers, $message);
    }

    function mailConfirm($address, $confirmString, $usernameString, $password) {
        $message = "<html><body>";
        $message .= "<h1>Welcome to MathLab</h1><br /><p>We can see that you have signed up, now just click <a href=\"http://localhost:8080/mathlab/processes/confirm.php?q=$confirmString&u=$usernameString\">here</a> to confirm your email.";
        $message .= "</body></html>";
        sendMail($address, "Welcome", $message, $password);
    }

    function mailResetPassword($address, $resetString, $usernameString, $password) {
        $message = "<html><body>";
        $message .= "<h1>Forgot your password?</h1><br /><p>We recently received a request to reset your password. If this was you click <a href=\"http://localhost:8080/mathlab/processes/new-password.php?q=$resetString&u=$usernameString\">here</a> to be guided through the process to reset your password. If this was not you, please ignore this email.";
        $message .= "</body></html>";
        sendMail($address, "Welcome", $message, $password);
    }

?>