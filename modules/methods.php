<?php

	// functions to get data without generating a warning or error if the data does not exist

	function getHeader($header) {
		if (isset($_GET[$header])) {
			return $_GET[$header];
		}
		return false;
	}

	function postHeader($header) {
		if (isset($_POST[$header])) {
			return $_POST[$header];
		}
		return false;
	}

	function getSession($str) {
		if (isset($_SESSION[$str])) {
			return $_SESSION[$str];
		}
		return false;
	}

	function maskEmail($email) {
		$parts = explode("@", $email);
		if (strlen($parts[0]) < 3) {
			if (strlen($parts[0]) == 2) {
				$parts[0] = "**";
			} else {
				$parts[0] = "*";
			}
		} else {
			for ($i = strlen($parts[0]) - 1; $i > strlen($parts[0]) - 4; $i--) {
				$parts[0][$i] = "*";
			}
		}
		for ($i = 0; $i < 3; $i++) {
			$parts[1][$i] = "*";
		}
		return $parts[0] . "*" . $parts[1];
	}
?>