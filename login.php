<?php

    // load essential build files

	require "modules/page-init.php";
	require "modules/navbar.php";

	// ends connection if user logged in

	if (getSession("user")) {
        header("Location: index.php");
		die();
    }

	// displays error if there was an issue with previous attempt or coming from profile page

	$error = getSession("error");
	if($error) {
		unset($_SESSION["error"]);
		$error = '<script type="text/javascript">
		window.commands.error = function () {
			presentModal("Error", "Bad username, password combination.");
		}
		</script>';
	} else if (getSession("origin") == "profile") {
		$error = '<script type="text/javascript">
		window.commands.error = function () {
			presentModal("Error", "You must be logged in to view your profile.");
		}
		</script>';
	}

	// generates basic page structure

	initHeader("Login", $default . $error);
	makeNav();
	initContent();

?>

<div class="card form-card">
	<div class="title center">Login</div>
	<div class="break-line"></div>
	<div class="text">
		<form action="processes/process-login.php" method="POST" class="login-form" autocomplete="off" spellcheck="false" name="login">
			<div class="form-block">
				<div class="left">
					<div class="inner">
						<label for="username">Username: </label>
					</div>
				</div>
				<div class="right">
					<div class="inner">
						<input type="text" id="username" name="username" placeholder="Username" autocomplete="username-auto" />
					</div>
				</div>
			</div>
			<div class="form-block">
				<div class="left">
					<div class="inner">
						<label for="password">Password: </label>
					</div>
				</div>
				<div class="right">
					<div class="inner">
						<input type="password" id="password" name="password" placeholder="Password" />
					</div>
				</div>
			</div>
			<div class="form-block">
				<div class="left">
					<div class="inner">
						<a href="sign-up.php">Don't have an account?</a>
					</div>
				</div>
				<div class="right">
					<div class="inner">
						<button type="submit">Login</button>
					</div>
				</div>
			</div>
			<div class="form-block">
				<div class="left">
					<div class="inner"></div>	
				</div>
				<div class="right">
					<div class="inner">
						<a href="forgot-password.php">Forgotten your password?</a>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<?php

	// close off page

	initFooter();

?>