<?php

    // load essential build files

	require "modules/page-init.php";
	require "modules/navbar.php";

	// redirects user to home if logged in

	if (getSession("user")) {
        header("Location: index.php");
        die();
    }

	// checks and displays present errors in previous requests

	$errors = getSession("error");
	if($errors) {
		unset($_SESSION["error"]);
		$errors = '<script type="text/javascript">
		window.commands.error = function () {
			triggerError("' . $errors . '");
		}
		</script>';
	}

	// generates basic page structure

	initHeader("Sign Up", $default . '<script type="text/javascript" src="js/sign-up.js"></script>' . $errors);
	makeNav();
	initContent();

?>

<div class="card form-card signup-card">
	<div class="title center">Sign Up</div>
	<div class="break-line"></div>
	<div class="text">
		<form action="processes/process-signup.php" method="POST" class="signup-form" autocomplete="off" spellcheck="false" name="signup">
            <div class="form-block">
				<div class="left">
					<div class="inner">
						<label for="fullname">Full Name:</label>
					</div>
				</div>
				<div class="right">
					<div class="inner">
						<input type="text" id="fullname" name="fullname" placeholder="Full Name" regex="^[a-zA-Z\s\-]*$" />
					</div>
				</div>
            </div>
            <div class="form-block">
				<div class="left">
					<div class="inner">
						<label for="target">Target Grade:</label>
					</div>
				</div>
				<div class="right">
					<div class="inner">
						<select id="target" name="target">
							<option value="null">Please Select...</option>
							<option value="Z">A*</option>
							<?php

								// cycle of all grades accepted
								
								for($i = 1; $i < count($validGrades); $i++) {
									echo '<option value="' . $validGrades[$i] . '">' . $validGrades[$i] . '</option>
									';
								}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-block">
				<div class="left">
					<div class="inner">
						<label for="username">Username:</label>
					</div>
				</div>
				<div class="right">
					<div class="inner">
						<input type="text" id="username" name="username" placeholder="Username" regex="^[a-zA-Z0-9_]*$" />
					</div>
				</div>
			</div>
            <div class="form-block">
				<div class="left">
					<div class="inner">
						<label for="email">Email:</label>
					</div>
				</div>
				<div class="right">
					<div class="inner">
						<input type="email" id="email" name="email" placeholder="Email" regex='^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$' />
					</div>
				</div>
			</div>
			<div class="form-block">
				<div class="left">
					<div class="inner">
						<label for="password">Password:</label>
					</div>
				</div>
				<div class="right">
					<div class="inner">
						<input type="password" id="password" name="password" placeholder="Password" />
					</div>
				</div>
			</div>
            <div class="form-block">
				<div class="left">
					<div class="inner">
						<label for="c-password">Confirm Password:</label>
					</div>
				</div>
				<div class="right">
					<div class="inner">
						<input type="password" id="c-password" name="c-password" placeholder="Confirm Password" />
					</div>
				</div>
			</div>
			<div class="form-block">
				<div class="left">
					<div class="inner">
						<a href="login.php">Already have an account? Click here.</a>
					</div>
				</div>
				<div class="right">
					<div class="inner">
						<button type="submit">Sign Up</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<?php

	// closes page with footer

	initFooter();

?>