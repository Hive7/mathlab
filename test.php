<?php

    // load essential build files

    require "modules/page-init.php";
    require "modules/methods.php";

    // fetches test percentages

    if(getSession("origin")) {
		if(!strpos($_SERVER["REQUEST_URI"], "login.php") && $_SESSION["origin"] == "profile") {
			unset($_SESSION["origin"]);
		} else if ($_SESSION["origin"] == "password" || $_SESSION["origin"] == "information") {
			unset($_SESSION["origin"]);
		}
	}

    $user = getSession("user");

    $extra = "var prevResults = [0, 0, 0];";

    if ($user) {
        
        require "modules/password.php";
        require "modules/database-commands.php";

        $query = "SELECT ALG, FAC, SEQ FROM SCORES WHERE USERID = ? LIMIT 5";

        $statement = sqlstatement($database, $query, array(array("i", $user)));
	    $statement->execute();

        $result = $statement->get_result();
        
        $alg = 0;
        $fac = 0;
        $seq = 0;

        while ($row = $result->fetch_array()) {
            $alg += 100 - $row[0];
            $fac += 100 - $row[1];
            $seq += 100 - $row[2];
        }

        $total = ($alg + $fac + $seq);

        // prevents zero division error

        if ($total == 0) {
            $total = 1;
        }

        $alg /= $total;
        $fac /= $total;
        $seq /= $total;

        $extra = "var prevResults = [$alg, $fac, $seq];";
    }

    // generates basic head data

    initHeader("Take a test!", "<script type=\"text/javascript\">$extra userID = \"" . getSession("user") . "\";</script>\n\t<script type=\"text/javascript\" src=\"js/test.js\"></script>\n\t<link rel=\"stylesheet\" href=\"css/test-style.css\" />\n\t<link rel=\"stylesheet\" href=\"css/modal-style.css\" />");

?>

<body>
    <div id="quiz-options">
        <div class="inner">
            <div class="title">How many questions do you want to do?</div>
            <div id="q15" class="qval">15</div>
            <div id="q30" class="qval">30</div>
        </div>
    </div>
    <div class="quiz-container" id="quiz">
        <div class="quiz">
            <div id="quiz-wrapper">
                <div id="question">?</div>
                <div id="answers">
                    <div class="answer answer1">
                        <div id="answer-val-1" class="answer-val">1.</div>
                    </div>
                    <div class="answer answer2">
                        <div id="answer-val-2" class="answer-val">2.</div>
                    </div>
                    <div class="answer answer3">
                        <div id="answer-val-3" class="answer-val">3.</div>
                    </div>
                    <div class="answer answer4">
                        <div id="answer-val-4" class="answer-val">4.</div>
                    </div>
                </div>
                <div class="counters">
                    <div id="c-question">1/20</div>
                    <div id="c-answer">0</div>
                </div>
            </div>
            <div id="output">
                <div id="score">
                    <div id="score-text"></div>
                    <div id="question-val"></div>
                    <div id="score-val"></div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>